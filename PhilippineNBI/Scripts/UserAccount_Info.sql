use PhilNBI
Go

set ansi_nulls on
Go

set quoted_identifier ON
Go

create Table dbo.UserAccount_Info (
  UserName nvarchar(50) not null, 
  UserPassword nvarchar(50) not null,
  LastName nvarchar(100) not null,
  MiddleName nvarchar(100) not null,
  GivenName nvarchar(100) not null,
  EmailAddress nvarchar(200) null,
  Department nvarchar(100) not null,
  IsAdministrator bit not null,
  IsNormalUser bit not null,
  IsDepartment1 bit not null,
  IsDepartment2 bit not null,
  IsDepartment3 bit not null,
  IsDepartment4 bit not null,
  IsDepartment5 bit not null,
  TimeAdded datetime not null,
  Remarks nvarchar(200) null
  constraint pk_indexid primary key clustered
  (
  UserName asc
  ) with (pad_index = OFF, ignore_dup_Key = OFF) ON [Primary]
  ) ON [primary]
    
  Go
  
  
  
  
